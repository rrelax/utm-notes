
# UTM
tl;dr: Due to licensing restrictions, team members cannot use Docker for Mac, and Mac computers using the Silicon CPU architecture struggle to run GitLab containers in alternate containerisation platforms* (such as Colima). 

UTM is offered as [an alternative solution](https://about.gitlab.com/handbook/support/workflows/test_env.html#utm-free--opensource---compatible-with-apple-silicon) in the support team member onboarding documents, however setting GitLab up in UTM is not yet fully documented ([this is the official UTM guidance](https://mac.getutm.app/gallery/ubuntu-20-04)). There are other available solutions that may suit you better, however if you're interested in using UTM this document may be of utility to you. 

Please let me know if any updates are required! This document was written using the following software versions:

| Software | Version | Notes |
| --- | --- | --- |
| UTM | 3.1.5 (53) | |
| Ubuntu Server (ARM)| 20.04 | | 
| GitLab | 14.9.3\-ee.0 | |

## Installation

### Setting Up Your Virtual Machine
0. Read the following [official UTM document](https://mac.getutm.app/gallery/ubuntu-20-04) - it doesn't fully document all the requisite steps, but provides some useful information regardless. 
1. Download the UTM dmg file from the [UTM Mac site](https://mac.getutm.app/) and install it/move the binary to your _Applications_ directory.
2. Download the [Ubuntu 20.04 ISO](https://ubuntu.com/download/server/arm) (for ARM-based architectures).

3. Open UTM and click the **+** button near the top of the UTM window. The following wizard will open:
![UTM VM Wizard Step 1](/images/utmInstall1.png)
4. Select _Virtualize_, then _Linux_.
5. Click _Browse_ and select the Ubuntu server (ARM) ISO file. Continue through the wizard, configuring the VM as desired.
6. I've assigned my virtual machine 4GB of RAM and the default setting for CPU cores. For disk space, I'd recommend assigning at least 20 GB of hard drive space, as 10GB has proven only _just_ enough for installing GitLab and Ubuntu server itself (Elasticsearch wouldn't fit as well with only 10GB!).
7. Click the _Play_ button to start the VM; after it boots you should be placed in the Ubuntu installation wizard.
8. Select _Install Ubuntu Server_.
9. Select your desired language setting, keyboard layout, leave the network configuration as it is (unless you'd like a custom configuration), leave the proxy config and mirror address as default, and use the default storage layout config (again, unless you prefer a different configuration)
10. The wizard will prompt you to confirm a _Destructive Action_ - since there is nothing in our virtual partitions, don't stress and just select  _Continue_.
11. Configure your profile as desired (I'd recommend picking a generic, easy to remember password and username)
12. Don't worry about the Ubuntu advantage token, continue.
13. When prompted, it's a good idea to install the OpenSSH server in case you need it for a later issue reproduction.
14. Disregard the _Featured Server Snaps_, continue.
15. Ubuntu Server will be installed - go get a coffee/tea/water, pat your pets, maybe consider where snails go when it's not raining or humid.
16. Once the installation is complete, it may be tempting to select _Reboot now_ - **I'd recommend doing the following step first!**
17. In the main UTM window, ensure the VM you've just created is selected, and scroll to the bottom of the right hand side pane. Select _CD/DVD_, and then the option _Clear_. This removes the ISO from the virtual CD/DVD drive, otherwise when the VM reboots the Ubuntu install wizard will begin again.
18. Return to the VM window, and press enter on the _Reboot Now_ option.
19. Once the reboot is complete, you should be prompted to provide your login credentials - success!

### Setting up GitLab on your VM
0. Firstly, inspect the [GitLab Installation docs](https://docs.gitlab.com/ee/install/) - specifically the one regarding installing the [Omnibus](https://docs.gitlab.com/omnibus/installation/).
1. Carry out the steps listed under the [Ubuntu guide](https://about.gitlab.com/install/#ubuntu), noting that you don't need to set the EXTERNAL_URL environment variable at this time.
5. Check your _root_ admin users initial password by using:
```bash
# This will print the initial, randomly generated admin user password
sudo cat /etc/gitlab/initial_root_password
```
### Post-install
- Test out [the Faker gem](https://gitlab.com/gitlab-com/support/support-training/blob/master/seed-data-api.md)!
- Review the [Next Steps document](https://docs.gitlab.com/ee/install/next_steps.html)

## Troubleshooting
### Installation Media ('install looping')
Remember to remove the installation media from the virtual CD-rom drive in UTM! If you don't, the virtual machine will display the '_install Ubuntu_' wizard to you upon every start-up.

### No Network connectivity

tl;dr: Sometimes UTM can change the network interface identifiers without updating the VM configuration itself. It's worth checking which interfaces are visible to the VM, and then seeing which interfaces the VM is configured to use.

#### Step 1: Which network interfaces are visible?
Check which network interfaces are available to the virtual machine using the following command:

``` bash
# This will display a list of network interface devices: 
ip link

# Output: 
# 1. lo: <LOOPBACK,UP,LOWER_UP> mtu....
#    link/lookback 00:00....
# 2: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu....
#    link/ether MA:CA:DD:RE:SS:...
```

#### Step 2: Check the interfaces are configured in the VM's Netplan files
Next, check which interfaces are configured on the virtual machine using the following command. If you don't see the ethernet address from the previous command listed, add it to the file (using your text editor of choice with **sudo**).

```bash
# This will display the _netplan_ interface configuration for the VM:
cat /etc/netplan/*.yaml

# Output: (comments below are part of the output)
# # This is the network config written by 'subiquity'
# network:
#   ethernets:
#     enp0s10:
#       dhcp4: true
#     enp0s11:
#       dhcp4: true
```

#### Step 3: Test the modified configuration
Test the new netplan configuration using the following command - it will give a countdown timer and prompt you to press _Enter_ to save the new configuration.- do so. If the new configuration is valid, you will receive the ouput "_Configuration Accepted_".

```bash
# This will test the new Netplan configuration
sudo netplan try
```
![netplanTry.png](/images/netplanTry.png)
![netplanTry.png](https://rrelax.gitlab.io/utm-notes/images/netplanTry.png)

#### Step 4: Apply the modified configuration
Apply the new configuration with the following command. Afterwards, test connectivity using `ping` and `dig` (a restart may be necessary)

```bash
# This will apply the modified configuration
sudo netplan apply
```

