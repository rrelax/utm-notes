# Summary

This repository contains [my](https://gitlab.com/rrelax) notes on setting UTM to run GitLab locally. I've consolidated these notes here as the available documentation for setting up and troubleshooting UTM is disparate across many sources (including the UTM site, GitHub issues, and stackoverflow).

MRs and issues are welcome!